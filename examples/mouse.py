#!/usr/bin/env python

# An example script using the rumba package

from rumba.model import *
from rumba.utils import ExperimentManager

# import testbed plugins
import rumba.testbeds.emulab as emulab
import rumba.testbeds.jfed as jfed
import rumba.testbeds.local as local
import rumba.testbeds.qemu as qemu

# import prototype plugins
import rumba.prototypes.ouroboros as our
import rumba.prototypes.rlite as rl
import rumba.prototypes.irati as irati

import rumba.log as log

log.set_logging_level('DEBUG')


n01 = NormalDIF("n01")

e01 = ShimEthDIF("e01")
e02 = ShimEthDIF("e02")
e03 = ShimEthDIF("e03")
e04 = ShimEthDIF("e04")
e05 = ShimEthDIF("e05")
e06 = ShimEthDIF("e06")
e07 = ShimEthDIF("e07")
e08 = ShimEthDIF("e08")
e09 = ShimEthDIF("e09")
e10 = ShimEthDIF("e10")
e11 = ShimEthDIF("e11")
e12 = ShimEthDIF("e12")
e13 = ShimEthDIF("e13")
e14 = ShimEthDIF("e14")
e15 = ShimEthDIF("e15")
e16 = ShimEthDIF("e16")
e17 = ShimEthDIF("e17")


a = Node("a",
         difs = [n01, e01, e06, e13 ],
         dif_registrations = {n01 : [e01, e06, e13]})

b = Node("b",
         difs = [n01, e01, e02, e04],
         dif_registrations = {n01 : [e01, e02, e04]})

c = Node("c",
         difs = [n01, e02, e03],
         dif_registrations = {n01 : [e02, e03]})

d = Node("d",
         difs = [n01, e03, e04, e05],
         dif_registrations = {n01 : [e03, e04, e05]})

e = Node("e",
         difs = [n01, e05, e06, e07],
         dif_registrations = {n01 : [e05, e06, e07]})

f = Node("f",
         difs = [n01, e07, e08],
         dif_registrations = {n01 : [e07, e08]})

g = Node("g",
         difs = [n01, e08, e09, e14],
         dif_registrations = {n01 : [e08, e09, e14]})

h = Node("h",
         difs = [n01, e09, e10, e15],
         dif_registrations = {n01 : [e09, e10, e15]})

i = Node("i",
         difs = [n01, e10, e11, e16],
         dif_registrations = {n01 : [e10, e11, e16]})

j = Node("j",
         difs = [n01, e11, e12],
         dif_registrations = {n01 : [e11, e12]})

k = Node("k",
         difs = [n01, e12, e13],
         dif_registrations = {n01 : [e12, e13]})

l = Node("l",
         difs = [n01, e14, e15],
         dif_registrations = {n01 : [e14, e15]})

m = Node("m",
         difs = [n01, e16, e17],
         dif_registrations = {n01 : [e16, e17]})

n = Node("n",
         difs = [n01, e17],
         dif_registrations = {n01 : [e17]})

tb = qemu.Testbed(exp_name = "mouse2")

exp = rl.Experiment(tb, nodes = [a, b, c, d, e, f, g, h, i, j, k, l, m, n])

print(exp)

with ExperimentManager(exp):
    exp.swap_in()
    exp.bootstrap_prototype()
