#!/usr/bin/env python

# Initial scale experiment for converged operator network
# (ARCFIRE T4.3)

from rumba.model import *
from rumba.storyboard import *
from rumba.utils import *

# import testbed plugins
import rumba.testbeds.emulab as emulab
import rumba.testbeds.jfed as jfed
import rumba.testbeds.local as local
import rumba.testbeds.qemu as qemu

# import prototype plugins
import rumba.prototypes.ouroboros as our
import rumba.prototypes.rlite as rl
import rumba.prototypes.irati as irati

log.set_logging_level('DEBUG')

f1 = NormalDIF("fixed")
l1 = NormalDIF("LTE")
m1 = NormalDIF("metro1")
m2 = NormalDIF("metro2")
m3 = NormalDIF("metro3")
c1 = NormalDIF("core")
n1 = NormalDIF("overlay")

f1e1 = ShimEthDIF("f1e1")
f1e2 = ShimEthDIF("f1e2")
f1e3 = ShimEthDIF("f1e3")

l1e1 = ShimEthDIF("l1e1")

m1f1 = ShimEthDIF("m1f1")

m1e1 = ShimEthDIF("m1e1")
m1e2 = ShimEthDIF("m1e2")
m1e3 = ShimEthDIF("m1e3")
m1e4 = ShimEthDIF("m1e4")
m1e5 = ShimEthDIF("m1e5")
m1e6 = ShimEthDIF("m1e6")

m2l1 = ShimEthDIF("m2l1")

m2e1 = ShimEthDIF("m2e1")
m2e2 = ShimEthDIF("m2e2")
m2e3 = ShimEthDIF("m2e3")
m2e4 = ShimEthDIF("m2e4")
m2e5 = ShimEthDIF("m2e5")
m2e6 = ShimEthDIF("m2e6")

m3e1 = ShimEthDIF("m3e1")
m3e2 = ShimEthDIF("m3e2")
m3e3 = ShimEthDIF("m3e3")
m3e4 = ShimEthDIF("m3e4")
m3e5 = ShimEthDIF("m3e5")
m3e6 = ShimEthDIF("m3e6")

m1c1 = ShimEthDIF("m1c1")
m1c2 = ShimEthDIF("m1c2")

m2c1 = ShimEthDIF("m2c1")
m2c2 = ShimEthDIF("m2c2")

m3c1 = ShimEthDIF("m3c1")
m3c2 = ShimEthDIF("m3c2")

c1e1 = ShimEthDIF("c1e1")
c1e2 = ShimEthDIF("c1e2")
c1e3 = ShimEthDIF("c1e3")
c1e4 = ShimEthDIF("c1e4")

f1n1 = Node("f1n1",
            difs = [n1, f1, f1e3],
            dif_registrations = {f1 : [f1e3], n1: [f1]})

f1n2 = Node("f1n2",
            difs = [f1, f1e1, f1e2, f1e3, m1f1],
            dif_registrations = {f1: [f1e1, f1e2, f1e3, m1f1]})

f1n3 = Node("f1n3",
            difs = [n1, f1, f1e1],
            dif_registrations = {f1: [f1e1], n1: [f1]})

f1n4 = Node("f1n4",
            difs = [n1, f1, f1e2],
            dif_registrations = {f1: [f1e2], n1: [f1]})

l1n1 = Node("l1n1",
            difs = [n1, l1, m2l1, l1e1],
            dif_registrations = {l1: [m2l1, l1e1], n1: [l1]})

l1n2 = Node("l1n2",
            difs = [n1, l1, l1e1],
            dif_registrations = {l1: [l1e1], n1: [l1]})

m1n1 = Node("m1n1",
            difs = [m1, m1e1, m1e6],
            dif_registrations = {m1: [m1e1, m1e6]})

m1n2 = Node("m1n2",
            difs = [m1, m1e1, m1e2],
            dif_registrations = {m1: [m1e1, m1e2]})
m1n3 = Node("m1n3",
            difs = [m1, m1e2, m1e3, m1c1],
            dif_registrations = {m1: [m1e2, m1e3, m1c1]})
m1n4 = Node("m1n4",
            difs = [m1, m1e3, m1e4, m1c2],
            dif_registrations = {m1: [m1e3, m1e4, m1c2]})
m1n5 = Node("m1n5",
            difs = [m1, m1e4, m1e5],
            dif_registrations = {m1: [m1e4, m1e5]})
m1n6 = Node("m1n6",
            difs = [n1, m1, f1, m1e5, m1e6, m1f1],
            dif_registrations = {m1: [m1e5, m1e6],
                                 f1: [m1f1], n1: [f1, m1]})

m2n1 = Node("m2n1",
            difs = [m2, m2e1, m2e6],
            dif_registrations = {m2: [m2e1, m2e6]})
m2n2 = Node("m2n2",
            difs = [m2, m2e1, m2e2],
            dif_registrations = {m2: [m2e1, m2e2]})
m2n3 = Node("m2n3",
            difs = [m2, m2e2, m2e3, m2c1],
            dif_registrations = {m2: [m2e2, m2e3, m2c1]})
m2n4 = Node("m2n4",
            difs = [m2, m2e3, m2e4, m2c2],
            dif_registrations = {m2: [m2e3, m2e4, m2c2]})
m2n5 = Node("m2n5",
            difs = [m2, m2e4, m2e5],
            dif_registrations = {m2: [m2e4, m2e5]})
m2n6 = Node("m2n6",
            difs = [n1, m2, l1, m2e5, m2e6, m2l1],
            dif_registrations = {m2: [m2e5, m2e6], l1: [m2l1], n1: [l1, m2]})

m3n1 = Node("m3n1",
            difs = [m3, m3e1, m3e6],
            dif_registrations = {m3: [m3e1, m3e6]})
m3n2 = Node("m3n2",
            difs = [m3, m3e1, m3e2],
            dif_registrations = {m3: [m3e1, m3e2]})
m3n3 = Node("m3n3",
            difs = [m3, m3e2, m3e3, m3c1],
            dif_registrations = {m3: [m3e2, m3e3, m3c1]})
m3n4 = Node("m3n4",
            difs = [m3, m3e3, m3e4, m3c2],
            dif_registrations = {m3: [m3e3, m3e4, m3c2]})
m3n5 = Node("m3n5",
            difs = [m3, m3e4, m3e5],
            dif_registrations = {m3: [m3e4, m3e5]})
m3n6 = Node("m3n6",
            difs = [m3, m3e5, m3e6],
            dif_registrations = {m3: [m3e5, m3e6]})

c1n1 = Node("c1n1",
            difs = [n1, c1, m1, m2, m1c1, m2c1, c1e1, c1e4],
            dif_registrations = {c1: [c1e1, c1e4], m1: [c1, m1c1],
                                 m2: [c1, m2c1], n1: [m1, m2]})
c1n2 = Node("c1n2",
            difs = [n1, c1, m2, m3, m2c2, m3c1, c1e1, c1e2],
            dif_registrations = {c1: [c1e1, c1e2], m2: [c1, m2c2],
                                 m3: [c1, m3c1], n1: [m2, m3]})
c1n3 = Node("c1n3",
            difs = [n1, c1, m3, m3c2, c1e2, c1e3],
            dif_registrations = {c1: [c1e2, c1e3], m3: [c1, m3c2], n1: [m3]})
c1n4 = Node("c1n4",
            difs = [n1, c1, m1, m1c2, c1e3, c1e4],
            dif_registrations = {c1: [c1e3, c1e4], m1: [c1, m1c2], n1: [m1]})

tb = jfed.Testbed(exp_name   = "arcfiret43",
                  proj_name  = "ARCFIRE",
                  cert_file  = "/home/dstaesse/jfed/cert.pem",
                  authority  = "exogeni.net",
                  username   = "dstaesse")

exp = rl.Experiment(tb, nodes = [f1n1, f1n2, f1n3, f1n4,
                                 l1n1, l1n2,
                                 m1n1, m1n2, m1n3, m1n4, m1n5, m1n6,
                                 m2n1, m2n2, m2n3, m2n4, m2n5, m2n6,
                                 m3n1, m3n2, m3n3, m3n4, m3n5, m3n6,
                                 c1n1, c1n2, c1n3, c1n4])

print(exp)

with ExperimentManager(exp, swap_out_strategy=PAUSE_SWAPOUT):
    exp.swap_in()
    exp.install_prototype()
    exp.bootstrap_prototype()
    c1 = Client("rinaperf", options ="-i 10000 -s 1000 -c 0 -d overlay",
                nodes=[f1n1, f1n3, f1n4, l1n2])
    s1 = Server("rinaperf", arrival_rate=2, mean_duration=5,
                options = "-l -d overlay", nodes = [c1n1], clients = [c1])
    sb = StoryBoard(experiment=exp, duration=3600, servers = [s1])
    sb.generate_script()
    sb.start()
