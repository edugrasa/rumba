Rumba is a Python framework that allows users to write Python scripts
to define recursive internet networks and run scripted experiments.

The official documentation can be found
[here](https://arcfire.gitlab.io/rumba/).
