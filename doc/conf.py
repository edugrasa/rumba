from datetime import datetime

html_theme = "sphinx_rtd_theme"
extensions = ['sphinx.ext.viewcode']

project = 'Rumba'
year = datetime.now().year
copyright = '2017-2018 imec, Nextworks S.r.l.'
author = 'Sander Vrijders, Marco Capitani, Dimitri Staessens, Vincenzo Maffione'
master_doc = 'index'
source_suffix = '.rst'
default_role = 'obj'
language = None

extensions.extend(['sphinx.ext.autodoc'])
autodoc_default_flags = ['members']

autoclass_content = 'both'

html_static_path = ['_static']
templates_path = ['_templates']

_locals = {}
with open('../rumba/_version.py') as fp:
    exec(fp.read(), None, _locals)
version = _locals['__version__']

version = version
release = version
