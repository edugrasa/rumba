Emulab
=============

`Emulab <https://www.emulab.net/>`_ is a network testbed, giving
researchers a wide range of environments in which to develop, debug,
and evaluate their systems.

Issues have been reported that Rumba asks for the password even
though a public key was added to the emulab interface. In this case
a workaround is to start an ssh-agent and add the public key there. ::

    $ eval `ssh-agent`
    $ ssh-add /home/user/.ssh/id_rsa.pub

.. automodule:: rumba.testbeds.emulab
    :member-order: bysource
    :show-inheritance:
    :inherited-members:
