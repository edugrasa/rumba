Prototype plugins
-------------------------

Rumba provides a generic Experiment class that specific protoypes can
derive from. All prototypes have to re-implement the functions
provided by the base class.

.. autoclass:: rumba.model.Experiment

Specific implementations of the Testbed class:

.. toctree::
    irati
    rlite
    ouroboros
