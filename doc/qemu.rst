QEMU
=============

`QEMU <http://wiki.qemu-project.org/Main_Page>`_ is a generic and open
source machine emulator and virtualizer.

In order to use the qemu testbed, the user should install the qemu and
bridge-utils packages on which the testbed depends: ::

     $ sudo apt-get install bridge-utils qemu

.. automodule:: rumba.testbeds.qemu
    :member-order: bysource
    :show-inheritance:
    :inherited-members:
