rlite
=============

`rlite <https://github.com/vmaffione/rlite>`_ is a lightweight Free
and Open Source implementation of the Recursive InterNetwork
Architecture (RINA) for GNU/Linux operating systems.

.. automodule:: rumba.prototypes.rlite
    :member-order: bysource
    :show-inheritance:
    :inherited-members:
