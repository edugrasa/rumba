Logging
=============

.. autofunction:: rumba.log.set_logging_level

.. autofunction:: rumba.log.reset_logging_level

.. autofunction:: rumba.log.flush_and_kill_logging

.. autofunction:: rumba.log.flush_log

.. autoclass:: rumba.log.LogOptions
