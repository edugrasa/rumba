====================================
Welcome to the Rumba documentation!
====================================

Rumba is a Python framework that allows users to write Python scripts
to define recursive internet networks and run scripted
experiments. First, Rumba creates a physical network on one of the
selected testbed plugins. If needed, Rumba can do an installation of
the selected prototype plugin on the testbed machines. The network is
then bootstrapped on the available nodes. Users can then run a
Storyboard which emulates real network traffic. Finally, the
experiment can be swapped out of the testbed.

This site covers Rumba's usage and API documentation. The repository
holding the code can be found `here
<https://gitlab.com/arcfire/rumba>`_. Example scripts can be found in
the examples/ folder.

Please report any bugs or issues `here
<https://gitlab.com/arcfire/rumba/issues>`_.

For general discussion of Rumba, please subscribe to our `mailing list
<https://www.freelists.org/list/rumba>`_: `rumba@freelists.org
<mailto:rumba@freelists.org>`_.

For day-to-day discussions, join our IRC chat: `#rumba
<irc://irc.freenode.net/rumba>`_.

Rumba is developed as part of the ARCFIRE project, funded by the
European commission under the H2020 framework.

.. toctree::
   general
   core
   testbed
   prototype
