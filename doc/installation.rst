Installation
************************

The easiest way to install Rumba is through pip. Rumba can be found on
the `PyPi <https://pypi.python.org/pypi/Rumba>`_ and can thus be
installed through pip. ::

    $ pip install rumba

Rumba supports both Python 2 and 3.

For Debian and Ubuntu, the following command will ensure that the
required dependencies are installed (replace python-dev with
python3-dev if using Python 3): ::

     $ apt-get install build-essential libssl-dev libffi-dev python-dev

To install the latest version from source, clone the repository and
install it from the command line: ::

     $ python setup.py install

Generating the documentation
---------------------------------

To generate the documentation, run the following command in the root
folder of the repository: ::

    $ sphinx-build doc/ public/

This will generate HTML pages in the public/ folder. Other output
formats are possible by using a different 'builder', the following
command would for instance generate LaTeX files: ::

    $ sphinx-build -b latex doc/ latex/
