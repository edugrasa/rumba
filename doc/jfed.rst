jFed
=============


`jFed <http://jfed.iminds.be/>`_ is a Java-based framework for
testbed federation.

In order to use the jFed testbed, a user first needs to download
his/her key from the `jFed authority
<https://authority.ilabt.iminds.be/](https://authority.ilabt.iminds.be/>`_.
After logging in, click on *Download your certificate*. Save the
contents in a file (for example cert.pem).

Before running Rumba you must run an SSH agent in same terminal. This
will also avoid you having to enter the passphrase for every login to
a node by the framework if you are not on an IPv6 enabled network.
(Apart from asking for the passphrase to login to the nodes, the
framework will always ask for the passphrase since it is needed by the
jFed CLI as well.) In order to start an SSH agent and to add the
certificate, type the following commands: ::

    $ eval `ssh-agent`
    $ ssh-add /home/morty/cert.pem

Pay attention to run your rumba script in the same terminal used
for the previous commands, without changing the user (e.g. using su
or sudo).

.. automodule:: rumba.testbeds.jfed
    :member-order: bysource
    :show-inheritance:
    :inherited-members:
